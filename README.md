# Volunteer together



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Clara132/volunteer-together.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Clara132/volunteer-together/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

## CHATGPT prompt template for implemeting features 

Implementation Prompt: "Volunteer together" Feature

Objective:
Implement a specific feature for the "Volunteer together" web application. Ensure adherence to the project constraints and requirements outlined in the specifications.

Feature:
[Specify the feature to be implemented]

Implementation Notes:

Design Considerations:

Follow the intuitive and user-friendly design principles outlined in the project specifications.
Ensure responsiveness for both desktop and mobile devices.
Technology Stack:

Utilize Django 4.1.4 framework for backend development.
Implement SQLite as the database for storing relevant data.
Incorporate Bootstrap for frontend development to achieve a cohesive and visually appealing interface.
Security Requirements:

Adhere to Django's default User management system for user-related functionalities.
Implement Django's default authentication and authorization systems for secure user access.
Integrate Django's default password reset system and customize it as per the project requirements.
User Interface:

Implement a web browser interface accessible on Windows.
Design a navigation bar with links for seamless page navigation.
Ensure that the content area presents information in a clear and organized manner.
Functionality:

Develop the feature considering the user roles (Admin, User) and their specific functionalities.
If applicable, incorporate messaging functionalities and notifications as outlined in the project specifications.
Database Interaction:

Implement database interactions according to the data models and structures specified in the project.
Ensure data integrity and uniqueness, especially in fields such as email addresses and job titles.
Usability and Learnability:

Focus on an intuitive design to enhance usability.
Provide clear instructions, tooltips, and user guides to facilitate ease of learning.
Testing:

Conduct thorough testing of the implemented feature to identify and rectify any bugs or issues.
Ensure compatibility and functionality across various web browsers.
Constraints and Assumptions:
[Include any specific constraints or assumptions relevant to the feature being implemented, as outlined in the project specifications.]

## All needed functionalities

      1---Enable and customize Django's default password reset functionality.
      
     2 ---Update the status of volunteering jobs based on admin decisions.
   
   3   ---Conduct usability testing to ensure ease of use.
   
   4   ---***mozda Offer a user guide or onboarding process for new users.

   5   -+-Develop a responsive web interface accessible via popular browsers.

    6  +- Use a responsive design to ensure usability on both desktop and mobile devices.
   
   7   ---Provide a messaging system within the web application.
         
   8   ---Implement a "Message Administrator" button on volunteering job pages.
         
    9  ---Create a form for users to compose and send messages to administrators.

    10  ---Display a list of volunteers on each volunteering job page.
      
   11   ---Notify administrators of user requests.
      
   12   ---Ensure uniqueness of the volunteering job title.
      
   13   --Allow users to remove their requests if needed. Odustani Btn
   
  14    --Integrate a messaging system for users to send inquiries to administrators.
   
     15 --Implement a notification system for users.
         
    16  ---Implement a search and filter functionality for volunteering jobs. FILTER treba

    17  ++----Provide tools to manage users and volunteering projects.
   
   18   ---Display the status of volunteering jobs on the user dashboard.

   19   +-+ Enable admins to view, edit, and delete user profiles and volunteering jobs.

      +++Include links to user profiles for additional information.
   
      +++Implement a "Request to join" button on available volunteering job pages.
        
      +++ Create an admin interface for posting new volunteering jobs.
         
      +++Include fields for title, description, location, and time.
      
      +++Implement routing functionality to handle page navigation.   

      +++Create a "Request Work" button for users on available volunteering job pages.
      
      +++Display user requests on the admin dashboard.

      +++Provide options for administrators to accept or reject requests.
   
      +++Include "Accept" and "Reject" buttons for each user request.

      +++Update the status of the volunteering job accordingly.
   
      +++Implement a "Finish Job" button for administrators on the admin dashboard.

      ++++Display messages on the admin dashboard.
      
      +++Create a user profile page displaying basic information.
         
      +++Include the user's name, email address, and other relevant details.
   
      +++List volunteering jobs requested and completed by the user on their profile.
      
      +++Develop an admin dashboard using the Django Admin interface.

      +++Display user requests on the admin dashboard.
   
      +++Create a user-friendly interface for users to apply for volunteering jobs.
         
      +++-Provide links to user and project profiles for additional information.

      +++Create a registration form with fields for name, email, and password.
      
      +++Ensure uniqueness of the email address.

      +++Develop a login form with fields for email and password.
   
      +++Validate email and password during the login process.
   
      +++Develop an admin dashboard using the Django Admin interface.
   
      +++Present user requests on the admin dashboard.
   
      +++Design a navigation bar for easy access to different sections.

      +++Create a well-organized content area for displaying relevant information.

      +++Set up a web server running on a Windows environment.
   
      +++Configure and utilize an SQLite database to store application data.
   
      +++Integrate Bootstrap for a responsive and visually appealing frontend.
   
      +++Leverage Django's default User model for user management.
   
      +++Implement and configure Django's default authentication and authorization.
   
      +++Design a navigation bar with links to different sections of the web application.




