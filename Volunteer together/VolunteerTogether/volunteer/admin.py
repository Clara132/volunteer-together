from django.contrib import admin

# VolunteerTogether/admin.py
from django.contrib import admin
from volunteer.models import VolunteeringJob, VolunteeringJobApplication, VolunteerRequest, Contact_message,Notification

class VolunteeringJobAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'location', 'reqVolunteer', 'numVolunteer', 'completed', 'date')


class VolunteeringJobApplicationAdmin(admin.ModelAdmin):
    list_display = ('volunteer', 'status')

class Contact_messageAdmin(admin.ModelAdmin):
    list_display = ('sender','respondant', 'content', 'timestamp', 'answered', 'admin_answered')


class NotificationAdmin(admin.ModelAdmin):
    list_display = ('sent_to','sent_from', 'content', 'is_read', 'timestamp')

admin.site.register(VolunteeringJob, VolunteeringJobAdmin)
admin.site.register(Contact_message, Contact_messageAdmin)
admin.site.register(Notification, NotificationAdmin)
admin.site.register(VolunteeringJobApplication, VolunteeringJobApplicationAdmin)
admin.site.register(VolunteerRequest)
# Register your models here.
