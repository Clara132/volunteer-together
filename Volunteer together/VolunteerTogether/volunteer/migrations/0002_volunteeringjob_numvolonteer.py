# Generated by Django 4.1.4 on 2024-01-26 05:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('volunteer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='volunteeringjob',
            name='numVolonteer',
            field=models.IntegerField(default=0),
        ),
    ]
