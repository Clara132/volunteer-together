# Generated by Django 4.1.4 on 2024-01-30 06:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("volunteer", "0009_volunteeringjob_date"),
    ]

    operations = [
        migrations.AddField(
            model_name="userprofile",
            name="is_volunteer",
            field=models.BooleanField(default=False),
        ),
    ]
