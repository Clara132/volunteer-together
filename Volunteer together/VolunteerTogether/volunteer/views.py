from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.contrib import messages
from .models import Contact_message, Notification
from .models import VolunteeringJob, VolunteeringJobApplication, VolunteerRequest
from .forms import PublishJobForm
from django.contrib import messages
from django.utils import timezone
from django.db.models import Max,Q

def home(request):
    if request.user.is_authenticated:
        jobs = VolunteeringJob.objects.filter(completed=False)
        user_applications = VolunteeringJobApplication.objects.filter(volunteer=request.user) 
        volunteer_request = VolunteerRequest.objects.filter(user_id=request.user)
        date_filter = request.GET.get('dateFilter')
        location_filter = request.GET.get('locationFilter')

        jobs = jobs.exclude(pk__in=user_applications.values('volunteering_job__id'))   
        jobs = VolunteeringJob.objects.filter(completed=False)

        if date_filter:
            jobs = jobs.filter(date=date_filter)

        if location_filter:
            jobs = jobs.filter(location__icontains=location_filter)

        user_applications = VolunteeringJobApplication.objects.filter(volunteer=request.user) 
        volunteer_request = VolunteerRequest.objects.filter(user_id=request.user)
        
        jobs = jobs.exclude(pk__in=user_applications.values('volunteering_job__id'))   

        context = {
            'user': request.user,
            'jobs': jobs,
            'user_applications': user_applications,
            'volunteer_request': volunteer_request,
        }
    else:
        context = {}
    
    return render(request, 'volunteer/home.html', context)

def apply_for_job(request, job_id):
    job = get_object_or_404(VolunteeringJob, pk=job_id)

    # Provjeri je li korisnik već prijavljen na ovaj posao
    existing_application = VolunteeringJobApplication.objects.filter(
        volunteer=request.user,
        volunteering_job=job,
    ).exists()

    if not existing_application:
        # Ako korisnik nije već prijavljen, stvori novu prijavu
        VolunteeringJobApplication.objects.create(
            volunteer=request.user,
            volunteering_job=job,
            status='pending',
        )

    return redirect('home')

@login_required
def profile(request):
    user = User.objects.get(pk=request.user.id)

    if request.user.is_authenticated:
        volunteer_request = VolunteerRequest.objects.filter(user_id=request.user)

    # Dohvati prijavljene radne akcije za trenutnog korisnika
    prijavljene_akcije = VolunteeringJobApplication.objects.filter(
        volunteer=request.user,
        status='accepted',
        volunteering_job__completed=False,
    ).values_list('volunteering_job__title', flat=True)

    obavljene_akcije = VolunteeringJob.objects.filter(
        volunteeringjobapplication__volunteer=user,
        volunteeringjobapplication__status='accepted',
        completed=True,
    )
    
    context = {
        'user': user,
        'accepted_volunteering_jobs': prijavljene_akcije,
        'completed_volunteering_jobs': obavljene_akcije,
        'volunteer_request': volunteer_request,
    }
    
    return render(request, 'volunteer/profile.html', context)

@login_required
def save_changes(request):
    user = User.objects.get(pk=request.user.id)

    if request.method == 'POST':
        user = request.user
        name = request.POST.get('name')
        surname = request.POST.get('surname')
        print("Received name:", name)
        print("Received surname:", surname)

        # Ažurirajte korisnički profil s novim podacima
        user.first_name = name
        user.last_name = surname
        user.save()

        return JsonResponse({'message': 'Changes saved successfully.'})

    return JsonResponse({'message': 'Invalid request method.'}, status=400)

def register(request):
    if request.method=="POST":
        
        username = request.POST.get('username')
        email = request.POST.get('email')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')
        is_volunteer = request.POST.get('is_volunteer') == 'on'
        
        if User.objects.filter(username=username).exists():
            return render(request, 'registration/register.html', {'error_message': 'Username is already taken'})
        
        if User.objects.filter(email=email).exists():
            return render(request, 'registration/register.html', {'error_message': 'Email is already taken'})
        
        if password1!=password2:
            return render(request, 'registration/register.html', {'error_message': 'Passwords do not match'})
        
        user = User(username=username, email=email, first_name = first_name, last_name = last_name, password = make_password(password1))
        user.save()
        
        if is_volunteer:
            volunteer_request = VolunteerRequest(user=user)
            volunteer_request.save()
        else:
            # Ako nije označen kao volonter, spremi običnog korisnika
            user.save()
        
        return redirect('login')

    return render(request, 'registration/register.html')

def job(request):
    context = {}
    return render(request, 'volunteer/publish_job.html', context)


@login_required
def publish_job(request):
    if request.method == 'POST':
        form = PublishJobForm(request.POST)
        if form.is_valid():
            # Create a new instance of VolunteeringJob
            VolunteeringJob.objects.create(
                title=form.cleaned_data['title'],
                description=form.cleaned_data['description'],
                reqVolunteer=form.cleaned_data['reqVolunteer'],
                location=form.cleaned_data['location'],
                date=form.cleaned_data['date']
            )

            # Show admin push notification
            messages.success(request, 'Job successfully created.')

            # Redirect to home.html
            return redirect('home')
    else:
        form = PublishJobForm()

    return render(request, 'volunteer/publish_job.html', {'form': form})

@login_required
def dashboard(request):
    if request.user.is_staff:
        jobs = VolunteeringJob.objects.filter(completed=False)
        context = {
            'jobs': jobs
        }
        
        return render(request, 'volunteer/dashboard.html', context)    
    
    else:
        return redirect('home')
    
@login_required
def dashboard_detail(request, job_id):
    if request.user.is_staff:
        job = get_object_or_404(VolunteeringJob, pk=job_id)
        volunteers = VolunteeringJobApplication.objects.filter(volunteering_job=job, status='pending')
        context = {
            'job': job,
            'volunteers': volunteers,
        }
        return render(request, 'volunteer/dashboard_detail.html', context)
    
    else:
        return redirect('home')

@login_required
def dashboard_accept(request, job_id, volunteer_id):
    if request.user.is_staff:
        job = get_object_or_404(VolunteeringJob, pk=job_id)
        volunteer_application = get_object_or_404(VolunteeringJobApplication, volunteering_job=job, volunteer_id=volunteer_id)

        if volunteer_application.status == 'pending':
            volunteer_application.status = 'accepted'
            volunteer_application.save()

            job.numVolunteer += 1
            if job.numVolunteer == job.reqVolunteer:
                job.completed = True
            job.save()

        return redirect('dashboard_detail', job_id=job_id)
    else:
        return redirect('home')

@login_required
def dashboard_deny(request, job_id, volunteer_id):
    if request.user.is_staff:
        job = get_object_or_404(VolunteeringJob, pk=job_id)
        volunteer_application = get_object_or_404(VolunteeringJobApplication, volunteering_job=job, volunteer_id=volunteer_id)
        volunteer_application.status = 'denied'
        volunteer_application.save()
    
        return redirect('dashboard_detail', job_id=job_id)
    else:
        return redirect('home')

@login_required
def finish_job(request, job_id):
    if request.user.is_staff:
        job = get_object_or_404(VolunteeringJob, pk=job_id)
        job.completed = True
        job.save()

        return redirect('dashboard')
    else:
        return redirect('home')
    
@login_required
def volunteer_detail(request):
    if request.user.is_staff:
        volunteers = VolunteerRequest.objects.filter(is_approved='pending')
        context = {
            'volunteers': volunteers
        }
        return render(request, 'volunteer/volunteer_detail.html', context)

@login_required
def volunteer_accept(request, volunteer_id):
    if request.user.is_staff:
        volunteer = get_object_or_404(VolunteerRequest, user_id=volunteer_id)
        volunteer.is_approved='accepted'
        volunteer.save()
        
        return redirect('volunteer_detail')
    else:
        return redirect('home')

@login_required
def volunteer_deny(request, volunteer_id):
    if request.user.is_staff:
        volunteer = get_object_or_404(VolunteerRequest, user_id=volunteer_id)
        volunteer.is_approved='denied'
        volunteer.save()
        
        return redirect('volunteer_detail')
    else:
        return redirect('home')




#admin inbox odabrani korisnik prebaciti ga na 
@login_required
def admins_inbox(request):
    # Retrieve messages for the current user (volunteer or admin)
    if request.user.is_staff:
        latest_messages = Contact_message.objects.filter(sender__is_staff=False).values('sender').annotate(last_message_time=Max('timestamp'))
        messages = Contact_message.objects.filter(sender__is_staff=False, timestamp__in=latest_messages.values('last_message_time'))
    
    context = {'messages': messages }
    return render(request, 'volunteer/admins_inbox.html', context)

@login_required
def save_Message(request):
    if request.method == 'POST':
        if request.user.is_staff:
            # Handle the admin's message submission
            content = request.POST.get('content')
            sender = request.user
            answered = True

            # Get all chat messages between the user and the admin
            messages = Contact_message.objects.filter(answered=False, sender__is_staff=False).annotate(last_message_time=Max('timestamp'))

            if messages.exists():  # Check if messages is not empty
                contactMessage = Contact_message(sender=sender, content=content, answered=answered, timestamp=timezone.now(), respondant=messages[0].sender)
                contactMessage.save()

                for message in messages:
                    message.answered = True
                    message.admin_answered = request.user
                    message.save()

                notification = Notification(
                    sent_to=messages[0].sender,
                    sent_from=sender,
                    timestamp=timezone.now(),
                    content=f"You have a new message from {sender.username}",
                    is_read=False
                )
                notification.save()

                return redirect('admins_inbox')
            else:
                contactMessage = Contact_message(sender=sender, content=content, answered=answered, timestamp=timezone.now())
                contactMessage.save()

                for message in messages:
                    message.answered = True
                    message.admin_answered = request.user
                    message.save()

                # Save a notification for the recipient
                notification = Notification(
                    sent_to=messages[0].sender,
                    sent_from=sender,
                    timestamp=timezone.now(),
                    content=f"You have a new message from {sender.username}",
                    is_read=False
                )
                notification.save()

                return redirect('admins_inbox')

        else:
            # Handle the user's message submission
            content = request.POST.get('content')
            sender = request.user

            # Save the message to the database
            contactMessage = Contact_message(sender=sender, content=content, timestamp=timezone.now())
            contactMessage.save()

            user = get_object_or_404(User, username=sender)
            messages = Contact_message.objects.filter(Q(sender=user) | Q(respondant=user)).order_by('timestamp')

            # Save a notification for all admins
            admin_recipients = User.objects.filter(is_staff=True)

            for admin_recipient in admin_recipients:
                notification = Notification(
                    sent_to=admin_recipient,
                    sent_from=sender,
                    timestamp=timezone.now(),
                    content=f"You have a new message from {sender.username}",
                    is_read=False
                )
            notification.save()

            context = {'messages': messages, 'user': user}
            return render(request, 'volunteer/chat_room.html', context)     
    
@login_required
def chat_room(request, username):
    if request.user.is_authenticated:
        user = get_object_or_404(User, username=username)

        # Get all chat messages where the username is the sender or respondant
        messages = Contact_message.objects.filter(Q(sender=user) | Q(respondant=user)).order_by('timestamp')

        context = {'messages': messages, 'user': user}
        return render(request, 'volunteer/chat_room.html', context)

@login_required
def admin_chat_room(request, username):
    if request.user.is_staff:
        
        user = get_object_or_404(User, username=username)

        # Get all chat messages where the username is the sender or respondant
        messages = Contact_message.objects.filter(Q(sender=user) | Q(respondant=user)).order_by('timestamp')

        context = {'messages': messages, 'user': user}
        return render(request, 'volunteer/admin_chat_room.html', context)
       

@login_required
def notifications(request):

    all_notifications = Notification.objects.filter(sent_to = request.user)
    
    context= {'notifications':all_notifications}
    return render(request, 'volunteer/notifications.html', context)

