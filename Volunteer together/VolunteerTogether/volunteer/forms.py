from django import forms

class PublishJobForm(forms.Form):
    title = forms.CharField(max_length=255, label='Naziv akcije')
    description = forms.CharField(widget=forms.Textarea, label='Opis')
    reqVolunteer = forms.IntegerField(label='Broj volontera')
    location = forms.CharField(max_length=255, label='Lokacija')
    date = forms.DateField(label='Datum održavanja', widget=forms.TextInput(attrs={'type': 'date'}))

class MessageForm(forms.Form):
    content = forms.CharField(widget=forms.Textarea(attrs={'rows': 3}), required=True)

