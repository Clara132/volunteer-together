from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    accepted_volunteering_jobs = models.ManyToManyField('VolunteeringJob', related_name='accepted_jobs')
    completed_volunteering_jobs = models.ManyToManyField('VolunteeringJob', related_name='completed_jobs')

class VolunteeringJob(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    location = models.CharField(max_length=255)
    numVolunteer = models.IntegerField(default = 0)
    reqVolunteer = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)
    location = models.CharField(max_length=255)
    date = models.DateField()
    #ostale stvari koje će pisati vezano uz akciju
    def __str__(self):
        return self.title
    
class VolunteeringJobApplication(models.Model):
    STATUS_CHOICES = [
        ('accepted', 'Accepted'),
        ('pending', 'Pending'),
        ('denied', 'Denied'),
    ]
    volunteering_job = models.ForeignKey(VolunteeringJob, on_delete=models.CASCADE)
    volunteer = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=8, choices=STATUS_CHOICES, default='pending')
    
    def __str__(self):
        return f"{self.volunteer.username} - {self.volunteering_job.title} ({self.status})"
    
class VolunteerRequest(models.Model):
    STATUS_CHOICES = [
        ('accepted', 'Accepted'),
        ('pending', 'Pending'),
        ('denied', 'Denied'),
    ]
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_approved = models.CharField(max_length=8, choices=STATUS_CHOICES, default='pending')
    
    def __str__(self):
        return self.user.username

class Contact_message(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    answered = models.BooleanField(default=False)
    reply = models.BooleanField(default=False)
    admin_answered = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='admin_answers')
    #keeps track who responded to this message
    respondant = models.ForeignKey(User, on_delete=models.CASCADE, related_name='received_messages', null=True) 

class Notification(models.Model):
    sent_to = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notifications_received')
    sent_from = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notifications_sent')
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
