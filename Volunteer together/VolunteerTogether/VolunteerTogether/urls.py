"""VolunteerTogether URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from volunteer import views

urlpatterns = [
    path("", views.home, name="home"),
    path("admin/", admin.site.urls),
    path("profile/", views.profile, name="profile"),
    path('save_changes/', views.save_changes, name='save_changes'),
    path("admins_inbox/", views.admins_inbox, name="admins_inbox"),
    path("accounts/", include('django.contrib.auth.urls')),
    path("register/", views.register, name="register"),
    path("job/", views.job, name="job"),
    path('publish_job/', views.publish_job, name='publish_job'),
    path('home/', views.home, name='home'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('dashboard/<int:job_id>/', views.dashboard_detail ,  name='dashboard_detail'),
    path('dashboard/<int:job_id>/accept/<int:volunteer_id>', views.dashboard_accept, name='dashboard_accept'),
    path('dashboard/<int:job_id>/deny/<int:volunteer_id>', views.dashboard_deny, name='dashboard_deny'),
    path('apply/<int:job_id>/', views.apply_for_job, name='apply_for_job'),
    path('dashboard/<int:job_id>/finish/', views.finish_job, name='finish_job'),
    path('dashboard/volunteer_detail/', views.volunteer_detail, name='volunteer_detail'),
    path('dashboard/volunteer_detail/volunteer_accept/<int:volunteer_id>/', views.volunteer_accept, name='volunteer_accept'),
    path('dashboard/volunteer_detail/volunteer_deny/<int:volunteer_id>/', views.volunteer_deny, name='volunteer_deny'),
    path('save_Message/', views.save_Message, name='save_Message'),
    path('chat_room/<str:username>/', views.chat_room, name='chat_room'),
    path('admin_chat_room/<str:username>/', views.admin_chat_room, name='admin_chat_room'),
    path('notifications/', views.notifications, name='notifications'),
]

